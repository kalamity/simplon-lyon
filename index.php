<!DOCTYPE html>
<html lang="en">
<head>
<?php include('sections/head.php') ?>
</head>
<body>
    <div id="main" class="container">
        <div id="wrapper">
            <div id='header'>
                <?php include('sections/header.php') ?>
            </div>
            <section class="col">
                <!--About Me section-->
                <div class="section row">
                    <div id="top" class="js-scrollTo title col">                    
                        <?php include('sections/about.php')?>
                    </div>
                </div>
                <!--Skill section-->
                <div class="section row">
                    <div id="skill" class="title col">
                       <?php include('sections/skills.php')?>
                    </div>
                <!--Porfolio section-->
                <div class=" js-scrollTo section row">
                <?php include('sections/portfolio.php') ?>
                </div>
            </section>
            <footer class="row">
                <div id="footer" class="col">
                    <!--<?php include('sections/footer.php')?>-->
                </div>
            </footer>
        </div>
    </div>
    
</body>

</html>
$(document).ready(function() {

    /**
     * Scroll
     */
    $('.js-scrollTo').on('click', function() { // Au clic sur un élément
        var page = $(this).attr('href'); // Page cible
        var speed = 750; // Durée de l'animation (en ms)
        $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
        return false;
    });
    /**
     * Glide
     */
var select = $('#options-type-select')
var glide = new Glide('#options-type', {
  type: select.value,
  focusAt: 'center',
  perView: 1
})

/*
select.addEventListener('change', function (event) {
  glide.update({
    type: event.target.value
  })
})
*/

glide.mount()
});


<h1>Skills</h1>
<div id="options-type" class="skillframe slider glide glide--ltr glide--slider glide--swipeable">
    <div class="slider__track glide__track" data-glide-el="track">
        <ul class="slider__slides glide__slides" style="transition: transform 800ms cubic-bezier(0.165, 0.84, 0.44, 1); width: 1540px; transform: translate3d(155px, 0px, 0px);">
            <!--Slide 1 Front-->
            <li class="slider__frame glide__slide glide__slide--active">
                <!--Skill-->
                <div class="row d-flex justify-content-center">
                    <div class="row">
                        <div class="skill">
                            <i class="devicon-html5-plain-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-css3-plain-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-javascript-plain"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-jquery-plain-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>

                        <div class="skill">
                            <i class="devicon-nodejs-plain-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-sass-original"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-angularjs-plain"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Title-->
                <div class="row">
                    <div class="col  d-flex justify-content-center">
                        <p>Front-end</p>
                    </div>
                </div>
            </li>
            <!--Slide 2 Back-->
            <li class="slider__frame glide__slide glide__slide--active">
                <!--Skill-->
                <div class="row d-flex justify-content-center">
                    <div class="row">
                        <div class="skill">
                            <i class="devicon-mysql-plain-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-php-plain"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                        <div class="skill">
                            <i class="devicon-symfony-original-wordmark"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Title-->
                <div class="row">
                    <div class="col  d-flex justify-content-center">
                        <p>Back-end</p>
                    </div>
                </div>
            </li>
            <!--Slide 3 Environements-->
            <li class="slider__frame glide__slide glide__slide--active">
                <!--Skill-->
                <div class="row d-flex justify-content-center">
                    <div class="skill">
                        <i class="devicon-git-plain-wordmark"></i>
                        <div clas="level">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="far fa-star"></i>
                        </div>
                    </div>
                    <div class="skill">
                        <i class="devicon-linux-plain"></i>
                        <div clas="level">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="skill">
                        <i class="devicon-windows8-original"></i>
                        <div clas="level">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                    <div class="skill">
                        <i class="devicon-docker-plain-wordmark"></i>
                        <div clas="level">
                            <i class="fas fa-star"></i>
                            <i class="far fa-star"></i>
                            <i class="far fa-star"></i>
                        </div>
                    </div>
                    <div class="skill">
                            <i class="devicon-photoshop-line"></i>
                            <div clas="level">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>

                </div>
                <!--Title-->
                <div class="row">
                    <div class="col  d-flex justify-content-center">
                        <p>System & Software</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="glide__arrows" data-glide-el="controls">
        <button class="btn btn-secondary glide__arrow glide__arrow--left" data-glide-dir="<">
            <i class="fas fa-arrow-left"></i>
        </button>
        <button class="btn btn-secondary glide__arrow glide__arrow--right" data-glide-dir=">">
            <i class="fas fa-arrow-right"></i>
        </button>
    </div>
</div>
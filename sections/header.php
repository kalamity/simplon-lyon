<!--Header-->
<div id="nav" class="col col_md-10">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link js-scrollTo" href="#top">Accueil
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scrollTo" href="#skill">Compétences</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scrollTo" href="#portfolio">Portfolio</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!--Header ends -->
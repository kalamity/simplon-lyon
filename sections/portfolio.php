<div id="portfolio" class="title col">
    <h1>Portfolio</h1>
    <h2>Advanced Projects</h2>
    <div class="row d-flex justify-content-center">
        <div class="box card" style="width: 15rem;">
            <a href="https://dogrates-84237.firebaseapp.com/">
                <img class="card-img-top" src="assets/images/dog.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="https://dogrates-84237.firebaseapp.com/">
                    <h5 class="card-title">Dog Rates - Angular 5 & Firebase</h3>
                        <p class="card-text">Site responsive Single page App avec une API Firebase </p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <a href="projets/meetup_view/index.php">
                <img class="card-img-top" src="assets/images/meetup.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="projets/meetup_view/index.php">
                    <h5 class="card-title">Meetups - Bootstrap, Js, PHP&MYSQL</h3>
                        <p class="card-text">API Front en Ajax communiquant avec une API Back (en MCV) </p>
                </a>
            </div>
        </div>
    </div>

    <h2>Beginner Projects</h2>
    <div class="row d-flex justify-content-center">
        
        <div class="box card" style="width: 15rem;">
            <a href="projets/todolist_db/index.php">
                <img class="card-img-top" src="assets/images/todo.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="projets/todolist_db/index.php">
                    <h5 class="card-title">Todo List - PHP & MySQL</h5>
                    <p class="card-text">Mini todolist utlisant PHP avec une base MySQL</p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <img class="card-img-top" src="assets/images/youmix.png" alt="Card image cap">
            <div class="card-body">
                <a href="projets/youMix">
                    <h5 class="card-title">You Mix - PHP, MySQL, JS & JQuery </h5>
                    <p class="card-text">Gestionnaire de list de vidéo Youtube</p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <a href="http://www.afgf.auchaudronpenche.com/">
                <img class="card-img-top" src="assets/images/afgf.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="http://www.afgf.auchaudronpenche.com/">
                    <h5 class="card-title">AFGF - Wordpress </h5>
                    <p class="card-text">Mini Projet Proposition de refonte du site de l'association "Familles Galactosémiques de France"</p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <a href="projets/giphy/index.html">
                <img class="card-img-top" src="assets/images/gifi.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="projets/giphy/index.html">
                    <h5 class="card-title">Giphy - HTML & CSS</h3>
                        <p class="card-text">Mockup du site Giphy en CSS3 et HTML5</p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <a href="projets/puissance4/index.html">
                <img class="card-img-top" src="assets/images/p4.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="projets/puissance4/index.html">
                    <h5 class="card-title">Puissance 4 - Js, HTML & CSS</h3>
                        <p class="card-text">Projet commun : Jeux de Puissance 4 en Js</p>
                </a>
            </div>
        </div>
        <div class="box card" style="width: 15rem;">
            <a href="projets/emojimemory/index.html">
                <img class="card-img-top" src="assets/images/emoji.png" alt="Card image cap">
            </a>
            <div class="card-body">
                <a href="projets/emojimemory/index.html">
                    <h5 class="card-title">Memory - JS</h5>
                    <p class="card-text">Jeux de memory utilisant Js et JQuery</p>
                </a>
            </div>
        </div>

    </div>
</div>
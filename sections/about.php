<div class="row about">
    <div class="pict col-sm-2">
        <img src="./assets/images/avatar.jpg" class="img-thumbnail" alt="avatar">
        <div class="social">
            <a href="https://gitlab.com/kalamity">
                <i class="fab fa-gitlab fa-2x" title="GitLab"></i>
            </a>
            <a href="https://www.linkedin.com/in/anne-liardet">
                <i class="fab fa-linkedin-in fa-2x" title="Linkedin"></i>
            </a>
            <a href="https://cvdesignr.com/public/5b04b0a8b490a" class="">
                <i class="fas fa-file fa-2x" title="CV"></i>
            </a>
        </div>
    </div>
    <div class="pict col-sm-4">
        <h3>Anne Liardet Développeuse Front-end</br> @Simplon_Lyon</h3>
    </div>

    <div class="col-sm-6">
        <p>Passionnée d’informatique et d'Internet depuis ses débuts (à la belle époque du modem 56k et des forfaits AOL 20h),
            j’ai décidé après 10 ans en tant que technicienne de support de me reconvertir dans le développement web. Je
            finis ma formation intensive Développeur.se Web spécialisation Front-end chez Simplon.co. fin juin et recherche
            un CDI à partir de mi-août.
        </p>
    </div>

</div>
//Board Game
//The board
board = [
    [0, 0, 0, 0, 0, 0], // Column 1
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
]

var a = 0;

function frame() {
    a++;
}

var h = 0;
var listToken = [0, 0, 0, 0, 0, 0, 0];

// function for each column
// for each column on click  the class of .token change, the var listToken helps to not overflow the board (add a max of 6 token)
function column(colNum) {
    var colon1 = document.getElementsByClassName("token" + colNum);
    var lourd = 6;
    var col = board[colNum - 1];
    var position = col.indexOf(0);
    if (listToken[colNum - 1] < lourd) {
        if (a % 2 == 0) {
            colon1[(lourd - 1) - position].classList.add("p");
            col[position] = 1;
        } else {
            colon1[(lourd - 1) - position].classList.add("ip");
            col[position] = 10;
        }
        listToken[colNum - 1]++;

    } else {
        a--;
    }
}


// Player function  : all the features that display palyer name, player turns
//Display a different color on click (if click are even or odds)
player1.classList.add("pa");
var player2 = document.getElementsByClassName("p_name2")[0];

function colorMan() {
    if (a % 2 == 0) {
        player1.classList.add("pa");
        player2.classList.remove("pp");
    } else {
        player2.classList.add("pp");
        player1.classList.remove("pa");
    }
}

//Display palyer names and save to the next page (index2.html)
function get_players_name() {
    var player_name1 = document.playersname.playername1.value;
    sessionStorage.setItem('name1', player_name1);
    if (player_name1 !== "") {
        var player_name2 = document.playersname.playername2.value;
        sessionStorage.setItem('name2', player_name2);
    } else {
        players_name();
    }
}

function display_player_name() {
    document.getElementsByClassName("p_name1")[0].innerHTML = sessionStorage.getItem("name1");
    document.getElementsByClassName("p_name2")[0].innerHTML = sessionStorage.getItem("name2");
}
